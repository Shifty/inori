"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.data_processing import user_avatar

kofi_url = 'https://ko-fi.com/shifty9'
paypal_url = 'https://www.paypal.me/shifty9'


async def donate(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    response = discord.Embed(color=0x2E005C)
    response.set_author(name='Kon Donation Information', icon_url=user_avatar(cmd.bot.user))
    response.description = f'Want to help? Support via [Ko-Fi]({kofi_url}) or [PayPal]({paypal_url})!'
    await pld.msg.channel.send(embed=response)

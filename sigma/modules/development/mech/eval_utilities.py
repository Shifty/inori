"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

getters = {}


class Unidentified(Exception):
    """
    An exception for handling NoneType return values.
    """

    def __init__(self, message):
        """
        :type message: str
        """
        self.message = message
        super().__init__(self.message)


def safe_get(name, oid):
    """
    :type name: str
    :type oid: int
    :rtype: mixed
    """
    getter = getters.get(name)
    if not getter:
        raise Unidentified("'{}' getter returned NoneType value".format(name))
    resp = getter(oid)
    if resp is None:
        raise Unidentified("'{}' returned NoneType value".format(getter.__name__))
    return resp


def set_getters(cmd, pld):
    """
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if getattr(pld.msg, 'guild') is None:
        return
    data = {
        'guild': cmd.bot.get_super().get_guild,
        'channel': pld.msg.guild.get_channel,
        'role': pld.msg.guild.get_role,
        'member': pld.msg.guild.get_member
    }
    getters.update(data)


def chn(cid):
    """
    :type cid:
    :rtype: discord.GuildChannel
    """
    return safe_get('channel', cid)


def gld(gid):
    """
    :type gid: int
    :rtype: discord.Guild
    """
    return safe_get('guild', gid)


def mem(mid):
    """
    :type mid: int
    :rtype: discord.Member
    """
    return safe_get('member', mid)


def role(rid):
    """
    :type rid: int
    :rtype: discord.Role
    """
    return safe_get('role', rid)

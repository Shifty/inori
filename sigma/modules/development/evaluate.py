﻿"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import importlib
import inspect
import os

import discord

from sigma.core.utilities.generic_responses import GenericResponse
# noinspection PyUnresolvedReferences
from sigma.modules.development.mech.eval_utilities import chn, gld, mem, role, set_getters

exe_icon = 'https://i.imgur.com/Lw7mmnX.png'
err_icon = 'https://i.imgur.com/S7aUuLU.png'


def generate_embed(error_text):
    """
    Generates a basic error embed.
    :type error_text: str
    :rtype: discord.Embed
    """
    response = discord.Embed(color=0xBE1931, description=error_text)
    response.set_author(name='Error', icon_url=err_icon)
    return response


def load_module_from_path(path):
    """
    Loads a module from a file path.
    :type path: str
    :rtype: module
    """
    success = True
    try:
        module = importlib.import_module(path)
        importlib.reload(module)
        return module, success
    except Exception as e:
        success = False
        return str(e), success


async def parse_func(cmd, pld):
    """
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    response = None
    func_text = pld.msg.content.split('\n')[1:-1]
    func_text = list(map(lambda x: '    ' + x, func_text))
    func_text.insert(0, 'from sigma.modules.development.mech.eval_utilities import chn, gld, mem, role')
    func_text.insert(1, 'async def main(cmd, pld):')
    with open(f'cache/{pld.msg.id}.py', 'w') as func_file:
        func_file.write('\n'.join(func_text))
    results, success = load_module_from_path(f'cache.{pld.msg.id}')
    if success:
        try:
            await results.main(cmd, pld)
            await pld.msg.add_reaction('✔')
        except Exception as e:
            response = generate_embed(str(e))
    else:
        response = generate_embed(results)
    os.remove(f'cache/{pld.msg.id}.py')
    if response:
        await pld.msg.channel.send(embed=response)


async def evaluate(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.args:
        set_getters(cmd, pld)
        if pld.args[0].startswith('```'):
            await parse_func(cmd, pld)
            return
        try:
            execution = " ".join(pld.args)
            output = eval(execution)
            if inspect.isawaitable(output):
                output = await output
            response = discord.Embed(color=0x38BE6E, description=f'```py\n{output}\n```')
            response.set_author(name='Executed', icon_url=exe_icon)
        except Exception as e:
            response = discord.Embed(color=0xBE1931, description=f'{e}')
            response.set_author(name='Error', icon_url=err_icon)
    else:
        response = GenericResponse('Nothing inputted.').error()
    await pld.msg.channel.send(embed=response)

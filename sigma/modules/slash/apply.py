"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord
from discord import TextStyle
from discord.ui import Modal, TextInput

from sigma.core.utilities.generic_responses import GenericResponse

invite = 'https://discordapp.com/invite/nSPWPxG'
gid = 138067606119645184
cid = 549046852658659332

data = {
    'rd': [
        'https://i.imgur.com/wzSr3i5.png',
        0xfa9407,
    ],
    'rda': [
        'https://i.imgur.com/goTilc0.png',
        0xd2691e,
    ],
    'rdu': [
        'https://i.imgur.com/BBcg4Qz.png',
        0x2f7fb5,
    ]
}


class ApplyModal(Modal, title='Royal Destiny Application'):
    def __init__(self, rd_guild):
        """
        :type rd_guild: discord.Guild
        """
        super().__init__(timeout=None)
        self.rd_guild = rd_guild

    name = TextInput(label='IGN', placeholder='What is your in-game name?', style=TextStyle.short)
    plat = TextInput(label='Platform', placeholder='What platform do you play on?', style=TextStyle.short)
    clan = TextInput(label='Clan', placeholder='Which clan? RD/RDU/RDA', style=TextStyle.short)
    mast = TextInput(label='Mastery Rank', placeholder='Include an L for Legendary ranks.', style=TextStyle.short)
    orig = TextInput(label='Reason', placeholder='Where did you hear about us?', style=TextStyle.paragraph)

    async def on_submit(self, inter):
        """
        inter: discord.Interaction
        """
        valid_platforms = [*data.keys(), 'royal destiny', 'royal destiny united', 'royal destiny academy']
        if self.clan.value.lower() not in valid_platforms:
            response = GenericResponse('Clan must be one of the following: `RD`, `RDU`, `RDA`').error()
            await inter.response.send_message(embed=response, ephemeral=True)

        else:
            for word in self.mast.value.split():
                if word.isdigit():
                    mastery = word
                    break
            else:
                response = GenericResponse('Mastery Rank must be a number.').error()
                await inter.response.send_message(embed=response, ephemeral=True)
                return

            try:
                icon, color = data[self.clan.value.lower()]
            except KeyError:
                icon = 'https://i.imgur.com/wzSr3i5.png'
                color = 0xfa940

            app = f'IGN: **{self.name.value}**\n'
            app += f'Platform: **{self.plat.value.upper()}**\n'
            app += f'Clan: **{self.clan.value.upper()}**\n'
            app += f'Mastery: **{mastery.upper()}**\n'
            app += f'Origin: **{self.orig.value}**'

            app_channel: discord.TextChannel = discord.utils.find(lambda x: x.id == cid, self.rd_guild.channels)
            app_response = discord.Embed(color=color, description=app, timestamp=arrow.utcnow().datetime)
            app_response.set_author(name=f'Application for {self.clan.value.upper()}', icon_url=icon)
            await app_channel.send(f'**Submitted By** {inter.user.mention}', embed=app_response)

            response = GenericResponse('Your application has been submitted.').ok()
            response.description = 'One of our recruiters will DM you when they\'re available.'
            response.set_footer(text='Please make sure you allow messages from server members.')
            await inter.response.send_message(embed=response, ephemeral=True)


async def apply(ev, inter):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param inter: The Interaction that triggered the event.
    :type inter: discord.Interaction
    """
    rd_guild = discord.utils.find(lambda x: x.id == gid, ev.bot.guilds)
    in_guild = discord.utils.find(lambda x: x.id == inter.user.id, rd_guild.members)
    if not in_guild:
        title = f'You must be a member of the [Royal Destiny]({invite}) Server to use this command.'
        response = GenericResponse(title).denied()
        await inter.response.send_message(embed=response, ephemeral=True)
    else:
        await inter.response.send_modal(ApplyModal(rd_guild))

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import errno
from io import BytesIO

import aiohttp
import discord

from sigma.core.utilities.data_processing import user_avatar
from sigma.modules.minigames.utils.ongoing.ongoing import Ongoing

try:
    # noinspection PyUnresolvedReferences
    from wand.image import Image
except ImportError:
    print('MagickWand shared library not found.')
    exit(errno.EINVAL)

from sigma.core.utilities.generic_responses import GenericResponse


def store_image(im):
    """
    Saves in image in memory.
    :type im: wand.image.Image
    :rtype: io.BytesIO
    """
    io = BytesIO()
    im.save(io)
    io.seek(0)
    return io


def liquify(image, mod, scale):
    """
    Applies the liquid_rescale filter to an image.
    :type image: wand.image.Image
    :type mod: float
    :type scale: int
    """
    image.liquid_rescale(
        width=int(image.width * mod),
        height=int(image.height * mod),
        delta_x=scale,
        rigidity=0
    )


async def magickwand(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    response = None
    if not Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
        Ongoing.set_ongoing(cmd.name, pld.msg.guild.id)
        if pld.args or pld.msg.attachments or pld.msg.mentions:
            img_data = None
            if pld.msg.attachments:
                image_url = pld.msg.attachments[0].url
            elif pld.msg.mentions:
                target = pld.msg.mentions[0]
                image_url = user_avatar(target, static=True)
            else:
                image_url = pld.args[0]
            try:
                async with aiohttp.ClientSession() as session:
                    async with session.get(image_url) as resp:
                        resp_type = resp.headers.get('Content-Type') or resp.headers.get('content-type')
                        if resp.status == 200 and resp_type.startswith('image'):
                            file_types = ['png', 'jpg', 'jpeg', 'webp']
                            if resp_type.split('/')[1].lower() in file_types:
                                img_data = await resp.read()
                                img_data = BytesIO(img_data)
            except aiohttp.InvalidURL:
                pass
            if img_data:
                with Image(file=img_data) as im:
                    if im.width <= 3000 and im.height <= 3000:
                        im.format = 'PNG'
                        im.transform(resize='800x800>')
                        size_str = f'{im.width}x{im.height}<'
                        liquify(im, 0.5, 1)
                        liquify(im, 1.5, 2)
                        im.transform(resize=size_str)
                        image = store_image(im)
                        file = discord.File(image, f'{pld.msg.id}.png')
                        await pld.msg.channel.send(file=file)
                    else:
                        response = GenericResponse('Image exceeds the maximum resolution of 3000x3000.').error()
            else:
                response = GenericResponse('Bad image. The supported formats are PNG, JPG, and WebP.').error()
        else:
            response = GenericResponse('Nothing inputted.').error()
        if Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
            Ongoing.del_ongoing(cmd.name, pld.msg.guild.id)
    else:
        response = GenericResponse('There is already one ongoing.').error()
    if response:
        await pld.msg.channel.send(embed=response)

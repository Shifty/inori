"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import asyncio

import arrow

decay_loop_running = False


async def image_decay_clockwork(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    global decay_loop_running
    if not decay_loop_running:
        decay_loop_running = True
        ev.bot.loop.create_task(image_decay_cycler(ev))


async def image_decay_cycler(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    while True:
        if ev.bot.is_ready():
            now = arrow.utcnow().float_timestamp
            decay_item = await ev.db.col.DecayingMessages.find_one({'decay_stamp': {'$lt': now}})
            if decay_item:
                sid = decay_item.get('server_id')
                cid = decay_item.get('channel_id')
                mid = decay_item.get('message_id')
                gld = await ev.bot.get_guild(sid)
                channel = gld.get_channel(cid)
                if channel:
                    # noinspection PyBroadException
                    try:
                        message = await channel.fetch_message(mid)
                        if message:
                            await message.delete()
                    except Exception:
                        pass
                    await ev.db.col.DecayingMessages.delete_one(decay_item)
        await asyncio.sleep(2)

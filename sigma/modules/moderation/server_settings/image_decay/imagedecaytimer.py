"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.data_processing import convert_to_seconds
from sigma.core.utilities.generic_responses import GenericResponse


async def imagedecaytimer(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.author.guild_permissions.manage_guild:
        decay_timer = await cmd.db.get_guild_settings(pld.msg.guild.id, 'image_decay_timer')
        if not decay_timer:
            await cmd.db.set_guild_settings(pld.msg.guild.id, 'image_decay_timer', 600)
            decay_timer = 600
        if pld.args:
            time_input = pld.args[0]
            try:
                time_sec = convert_to_seconds(time_input)
                await cmd.db.set_guild_settings(pld.msg.guild.id, 'image_decay_timer', time_sec)
                response = GenericResponse('Image decay timer was updated.').ok()
            except (LookupError, ValueError):
                response = GenericResponse('Please use the format HH:MM:SS.').error()
        else:
            response = discord.Embed(color=0xF9F9F9, title=f'🕐 Image decay is `{decay_timer}` seconds')
    else:
        response = GenericResponse('Access Denied. Manage Server needed.').denied()
    await pld.msg.channel.send(embed=response)

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord


async def image_checker(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.MessagePayload
    """
    if pld.msg.guild:
        if isinstance(pld.msg.author, discord.Member):
            if pld.msg.author.id not in ev.bot.cfg.dsc.owners:
                if not pld.msg.channel.permissions_for(pld.msg.author).administrator:
                    if pld.msg.attachments:
                        decay_list = await ev.db.get_guild_settings(pld.msg.guild.id, 'image_decay')
                        decay_timer = await ev.db.get_guild_settings(pld.msg.guild.id, 'image_decay_timer') or 600
                        if decay_list is None:
                            decay_list = []
                        if pld.msg.channel.id in decay_list:
                            decay_data = {
                                'server_id': pld.msg.guild.id,
                                'message_id': pld.msg.id,
                                'channel_id': pld.msg.channel.id,
                                'decay_stamp': arrow.utcnow().int_timestamp + decay_timer
                            }
                            await ev.db.col.DecayingMessages.insert_one(decay_data)

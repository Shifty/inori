"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sigma.core.utilities.generic_responses import GenericResponse


async def toggleimagedecay(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.author.guild_permissions.manage_guild:
        if pld.msg.channel_mentions:
            target = pld.msg.channel_mentions[0]
            decay_list = await cmd.db.get_guild_settings(pld.msg.guild.id, 'image_decay')
            if decay_list is None:
                decay_list = []
            if target.id in decay_list:
                decay_list.remove(target.id)
                decay_coll = cmd.db.col.DecayingMessages
                server_data = await decay_coll.find({'channel_id': pld.msg.channel.id}).to_list(None)
                for item in server_data:
                    await decay_coll.delete_one({'message_id': item.get('message_id')})
                response = GenericResponse(f'#{target.name} no longer has an image decay.').ok()
            else:
                decay_list.append(target.id)
                response = GenericResponse(f'#{target.name} now has an image decay.').ok()
            await cmd.db.set_guild_settings(pld.msg.guild.id, 'image_decay', decay_list)
        else:
            response = GenericResponse('No channel targeted.').error()
    else:
        response = GenericResponse('Access Denied. Manage Server needed.').denied()
    await pld.msg.channel.send(embed=response)

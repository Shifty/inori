"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import asyncio
import secrets

import arrow
import discord

rainbow_loop_running = False


async def rainbow_clockwork(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    global rainbow_loop_running
    if not rainbow_loop_running:
        rainbow_loop_running = True
        ev.bot.loop.create_task(rainbow_cycler(ev))


async def rainbow_cycler(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    while True:
        if ev.bot.is_ready():
            settings_coll = ev.db.col.ServerSettings
            guilds = await settings_coll.find({'rainbow_role.state': True}).to_list(None)
            for guild in guilds:
                rainbow_role = guild.get('rainbow_role', {})
                execution_stamp = rainbow_role.get('cycle_stamp')
                current_stamp = arrow.utcnow().int_timestamp
                if current_stamp > execution_stamp:
                    colors = rainbow_role.get('colors')
                    if colors:
                        color = secrets.choice(colors)
                        guild_id = rainbow_role.get('server_id')
                        role_id = rainbow_role.get('role_id')
                        guild = await ev.bot.get_guild(guild_id)
                        if guild:
                            role = guild.get_role(role_id)
                            if role and guild.me.top_role.position > role.position:
                                rainbow_role.update({'cycle_stamp': arrow.utcnow().int_timestamp + 300})
                                update_data = {'$set': {'rainbow_role': rainbow_role}}
                                try:
                                    await role.edit(color=discord.Color(color))
                                    await settings_coll.update_one({'server_id': guild_id}, update_data)
                                except (discord.NotFound, discord.Forbidden):
                                    pass
        await asyncio.sleep(2)

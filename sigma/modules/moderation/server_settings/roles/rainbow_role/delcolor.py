"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sigma.core.utilities.generic_responses import GenericResponse


async def delcolor(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.author.guild_permissions.manage_roles:
        if pld.args:
            bad_hex = False
            hex_req = pld.args[0].lower().strip('#')
            if len(hex_req) == 3:
                hex_req = hex_req * 2
            if len(hex_req) != 6:
                bad_hex = True
            if not bad_hex:
                color_int = int(f'0x{hex_req}', 16)
                rainbow_role = pld.settings.get('rainbow_role', {})
                if rainbow_role.get('state'):
                    colors = rainbow_role.get('colors')
                    if color_int in colors:
                        rainbow_role.get('colors').remove(color_int)
                        await cmd.db.set_guild_settings(pld.msg.guild.id, 'rainbow_role', rainbow_role)
                        response = GenericResponse(f'{hex_req.upper()} was removed from the Rainbow.').ok()
                    else:
                        response = GenericResponse(f'{hex_req.upper()} wasn\'t in the Rainbow.').error()
                else:
                    response = GenericResponse('The Rainbow role is not enabled.').error()
            else:
                response = GenericResponse('Invalid HEX color code.').error()
        else:
            response = GenericResponse('Nothing inputted.').error()
    else:
        response = GenericResponse('Access Denied. Manage Roles needed.').denied()
    await pld.msg.channel.send(embed=response)

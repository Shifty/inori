"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse


async def rainbowrole(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.author.guild_permissions.manage_roles:
        rainbow_role = pld.settings.get('rainbow_role', {})
        if rainbow_role:
            if rainbow_role.get('state'):
                rainbow_role.update({"state": False})
                await cmd.db.set_guild_settings(pld.msg.guild.id, 'rainbow_role', rainbow_role)
                try:
                    role = pld.msg.guild.get_role(rainbow_role.get('role_id'))
                    if role:
                        await role.delete()
                except (discord.NotFound, discord.Forbidden):
                    pass
                response = GenericResponse('The Rainbow role has been disabled.').ok()
            else:
                role = await pld.msg.guild.create_role(name="Rainbow")
                rainbow_role.update({'role_id': role.id, 'state': True})
                await cmd.db.set_guild_settings(pld.msg.guild.id, 'rainbow_role', rainbow_role)
                response = GenericResponse('The Rainbow role has been enabled.').ok()
        else:
            role = await pld.msg.guild.create_role(name="Rainbow")
            rainbow_role = {
                'server_id': pld.msg.guild.id,
                'role_id': role.id,
                'cycle_stamp': (arrow.utcnow().int_timestamp + 300),
                'state': True,
                'colors': []
            }
            await cmd.db.set_guild_settings(pld.msg.guild.id, 'rainbow_role', rainbow_role)
            response = GenericResponse('The Rainbow role has been enabled.').ok()
    else:
        response = GenericResponse('Access Denied. Manage Roles needed.').denied()
    await pld.msg.channel.send(embed=response)

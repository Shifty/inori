"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.data_processing import get_image_colors
from sigma.core.utilities.generic_responses import GenericResponse


async def rainbowcolors(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    rainbow_role = pld.settings.get('rainbow_role', {})
    if rainbow_role:
        colors = rainbow_role.get('colors')
        guild_icon = str(pld.msg.guild.icon.url) if pld.msg.guild.icon else None
        response = discord.Embed(color=await get_image_colors(guild_icon))
        if not len(colors) == 0:
            rainbow_text = ''
            for c in colors:
                color = str(hex(c))[2:].upper()
                while len(color) < 6:
                    color = '0' + color
                rainbow_text += f'- #{color}\n'
            response.set_footer(text=f'View these colors with {cmd.bot.cfg.pref.prefix}color.')
            response.set_author(name=pld.msg.guild.name, icon_url=guild_icon)
            response.add_field(name='Rainbow role colors', value=f'{rainbow_text}')
        else:
            response = GenericResponse('The Rainbow role doesn\'t have any colors.').warn()
    else:
        response = GenericResponse('There is no rainbow role on this server.').not_found()
    await pld.msg.channel.send(embed=response)

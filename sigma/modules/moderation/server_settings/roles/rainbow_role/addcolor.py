"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sigma.core.utilities.generic_responses import GenericResponse


async def addcolor(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.author.guild_permissions.manage_roles:
        if pld.args:
            hex_req = pld.args[0].lower().strip('#')
            if len(hex_req) == 3:
                hex_req = hex_req * 2
            try:
                color_int = int(f'0x{hex_req}', 16)
            except ValueError:
                color_int = None
            if color_int:
                rainbow_role = pld.settings.get('rainbow_role', {})
                if rainbow_role.get('state'):
                    colors = rainbow_role.get('colors')
                    if len(colors) < 15:
                        if color_int in colors:
                            response = GenericResponse(f'{hex_req.upper()} is already in the Rainbow.').error()
                        else:
                            rainbow_role.get('colors').append(color_int)
                            await cmd.db.set_guild_settings(pld.msg.guild.id, 'rainbow_role', rainbow_role)
                            response = GenericResponse(f'{hex_req.upper()} was added to the Rainbow.').ok()
                    else:
                        response = GenericResponse('You can only have up to 15 colors.').error()
                else:
                    response = GenericResponse('The Rainbow role is not enabled.').warn()
            else:
                response = GenericResponse('Invalid HEX color code.').error()
        else:
            response = GenericResponse('Nothing inputted.').error()
    else:
        response = GenericResponse('Access Denied. Manage Roles needed.').denied()
    await pld.msg.channel.send(embed=response)

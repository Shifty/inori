"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import datetime
import os
import socket
import sys

import arrow
import discord
import humanfriendly
import psutil

from sigma.core.utilities.generic_responses import GenericResponse
from sigma.modules.utilities.information.status import get_os_icon


def get_shard_latency(latencies, shard_id):
    """
    :type latencies: list
    :type shard_id: int
    :rtype: int
    """
    shard_latency = None
    for lat_sd, lat_ms in latencies:
        if lat_sd == shard_id:
            shard_latency = lat_ms
            break
    return int(shard_latency * 1000)


modes = [
    'ram', 'mem', 'memory', 'name',
    'uptime', 'cpu', 'processor'
]


async def system(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.args:
        mode = pld.args[0].lower()
        if mode == 'cpu':
            mode_name = 'Processor'
            cpu_clock = psutil.cpu_freq()
            cpu_clock = round(cpu_clock.current, 2) if cpu_clock else '???'
            desc = f'Count: **{psutil.cpu_count()} ({psutil.cpu_count(logical=False)})**'
            desc += f'\nUsage: **{psutil.cpu_percent()}%**'
            desc += f'\nClock: **{cpu_clock} MHz**'
        elif mode == 'ram':
            mode_name = 'Memory'
            avail_mem = psutil.virtual_memory().available
            total_mem = psutil.virtual_memory().total
            used_mem = humanfriendly.format_size(total_mem - avail_mem, binary=True)
            total_mem = humanfriendly.format_size(total_mem, binary=True)
            sigma_mem = humanfriendly.format_size(psutil.Process(os.getpid()).memory_info().rss, binary=True)
            desc = f'Me: **{sigma_mem}**'
            desc += f'\nUsed: **{used_mem}**'
            desc += f'\nTotal: **{total_mem}**'
        elif mode == 'name':
            mode_name = 'Name'
            host_name = socket.gethostname()
            platform = sys.platform.upper()
            desc = f'Host: **{host_name}**'
            desc += f'\nPlatform: **{platform}**'
        elif mode == 'uptime':
            mode_name = 'Up Time'
            now = arrow.utcnow().int_timestamp
            sys_offset = now - psutil.boot_time()
            sys_uptime = str(datetime.timedelta(seconds=sys_offset))
            bot_offset = now - cmd.bot.start_time.int_timestamp
            bot_uptime = str(datetime.timedelta(seconds=bot_offset))
            desc = f'System: **{sys_uptime}**'
            desc += f'\nBot: **{bot_uptime}**'
        else:
            mode_name, desc = None, None
        if mode_name:
            os_icon, os_color = get_os_icon()
            response = discord.Embed(color=os_color, description=desc)
            response.set_author(name=mode_name, icon_url=os_icon)
        else:
            response = GenericResponse('Invalid mode.').error()
    else:
        response = GenericResponse('Nothing inputted.').error()
    await pld.msg.channel.send(embed=response)

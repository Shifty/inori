"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.generic_responses import GenericResponse


def get_output(message, args):
    """
    :type message: discord.Message
    :type args: list
    :rtype: str, str
    """
    if ';;' in message.content:
        post_qry = (' '.join(args)).split(';;')
        lookup = ''.join(post_qry[0]).lower().strip()
        output = ' '.join(post_qry[1:]).strip()
    else:
        lookup = ' '.join(args).lower()
        output = ''
    if output:
        output = output if output.startswith('\n') else '\n' + output
    return lookup, output


async def mention(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    response = None
    if pld.msg.channel.permissions_for(pld.msg.author).manage_messages:
        if pld.args:
            lookup, output = get_output(pld.msg, pld.args)
            if str.isdigit(lookup):
                role_search = pld.msg.guild.get_role(int(lookup))
            else:
                role_search = discord.utils.find(lambda x: x.name.lower() == lookup, pld.msg.guild.roles)
            if role_search:
                role_below = pld.msg.guild.me.top_role > role_search
                if role_below:
                    role_mention = role_search.mention + f' ~ {pld.msg.author.name}'
                    try:
                        await pld.msg.delete()
                    except discord.NotFound:
                        pass
                    if not role_search.mentionable:
                        await role_search.edit(mentionable=True)
                        await pld.msg.channel.send(role_mention + output)
                        await role_search.edit(mentionable=False)
                    else:
                        await pld.msg.channel.send(role_mention + output)
                else:
                    response = GenericResponse('This role is above my highest role.').error()
            else:
                response = GenericResponse(f'{lookup[:25]} not found.').not_found()
        else:
            response = GenericResponse('Nothing inputted.').error()
    else:
        response = GenericResponse('Access Denied. Manage Messages needed.').denied()
    if response:
        await pld.msg.channel.send(embed=response)

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.generic_responses import GenericResponse
from sigma.modules.owner_controls.custom.events.nodes.event_data import EventData


def parse_data(row):
    """
    :type row: list
    :rtype: str
    """
    row.extend(['-' for _ in range(8 - len(row))])
    row_data = []
    for cell in row:
        if cell in ['-', '']:
            cell = 'None'
        row_data.append(cell)
    row_out = f'**IGN:** {row_data[0]}'
    row_out += f'\n**Clan:** {row_data[2]}'
    row_out += f'\n**Responsible:** {row_data[3]}'
    row_out += f'\n**Reason:** {row_data[4]}'
    row_out += f'\n**Screenshot:** {row_data[5]}'
    row_out += f'\n**Note:** {row_data[6]}'
    row_out += f'\n**Duration:** {row_data[7]}'
    return row_out


async def banchecker(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.msg.guild.id == 138067606119645184:
        data = EventData(cmd.bot.cfg.ggl)
        if pld.args:
            mem_ign = pld.args[0].lower()
            sheet_data = data.banned(mem_ign, pld.msg.mentions, True)
            if sheet_data is not None:
                banned, user_data = sheet_data
                if banned and user_data:
                    row_text = parse_data(user_data)
                    response = discord.Embed(color=0x696969, title='🔐 This user is banned')
                    response.add_field(name='Details', value=row_text)
                else:
                    response = GenericResponse('That user is not banned.').ok()
            else:
                response = GenericResponse('Google Sheets API currently unavailable.').error()
        else:
            response = GenericResponse('Nothing inputted.').error()
    else:
        response = GenericResponse('This command can only be used on the Royal Destiny server.').denied()
    await pld.msg.channel.send(embed=response)

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord


def get_mem_count(gld):
    """
    :type gld: discord.Guild
    :rtype: dict
    """
    states = {
        'dnd': 0, 'idle': 0, 'invisible': 0, 'offline': 0,
        'online': 0, 'members': 0, 'active': 0, 'bots': 0
    }
    for member in gld.members:
        if not member.bot:
            state = member.status.name
            states.update({state: states.get(state) + 1})
            states.update({'members': states.get('members') + 1})
            if state != 'offline':
                states.update({'active': states.get('active') + 1})
        else:
            states.update({'bots': states.get('bots') + 1})
    return states


async def members(_cmd, pld):
    """
    :param _cmd: The command object referenced in the command.
    :type _cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    states = get_mem_count(pld.msg.guild)
    active = round(((states['active'] / states['members']) * 100), 2)
    other = states['idle'] + states['dnd']
    mem_info = f'\nMembers: **{states["members"]}**'
    mem_info += f'\nOnline: **{states["online"]}**'
    mem_info += f'\nOffline: **{states["offline"]}**'
    mem_info += f'\nOther: **{other}**'
    mem_info += f'\nBots: **{states["bots"]}**'
    mem_info += f'\nActive: **{active}%**'
    response = discord.Embed(color=0x2E005C)
    response.add_field(name='👥 Member Status', value=mem_info)
    response.set_footer(text='Active is users who are not offline')
    await pld.msg.channel.send(embed=response)

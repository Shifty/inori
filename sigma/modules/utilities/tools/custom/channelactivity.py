"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse
from sigma.modules.minigames.utils.ongoing.ongoing import Ongoing


async def get_history(channel, limit, time):
    """
    :type channel: discord.GuildChannel
    :type limit: int
    :type time: datetime.datetime
    :rtype: int, int
    """
    unique = []
    channel_hist = [m async for m in channel.history(limit=limit, after=time)]
    for log in channel_hist:
        if log.author.id not in unique:
            unique.append(log.author.id)
    return len(channel_hist), len(unique)


async def make_init_message(channel):
    """
    :type channel: discord.GuildChannel
    :rtype: discord.Message
    """
    init_response = discord.Embed(color=0x744eaa, title='📑 Collecting Data...')
    init_message = await channel.send(embed=init_response)
    return init_message


async def channelactivity(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if not Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
        if not pld.args:
            target = pld.msg.channel
            limit = "7"
        else:
            target = pld.msg.channel_mentions[0] if pld.msg.channel_mentions else pld.msg.channel
            limit = pld.args[-1]
        me = pld.msg.guild.me
        if limit.isdigit() and limit in [str(i) for i in range(1, 8)]:
            if target.permissions_for(me).read_messages and target.permissions_for(me).read_message_history:
                Ongoing.set_ongoing(cmd.name, pld.msg.guild.id)
                init_message = await make_init_message(pld.msg.channel)
                time_diff = arrow.get(arrow.utcnow().int_timestamp - (int(limit) * 86400)).naive
                count, unique = await get_history(target, 100000, time_diff)
                response = discord.Embed(color=0x744eaa)
                value = f'Total Messages: {count}\nUnique Users: {unique}'
                response.add_field(name=f'📑 #{target.name} Activity', value=value)
                if count >= 100000:
                    response.set_footer(text='Limit of 100000 messages.')
                await init_message.edit(embed=response)
                if Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
                    Ongoing.del_ongoing(cmd.name, pld.msg.guild.id)
                return
            else:
                response = GenericResponse('I can\'t read that channel\'s history.').error()
        else:
            response = GenericResponse('Limit must be a number 1-7.').error()
    else:
        response = GenericResponse('There is already one ongoing.').error()
    await pld.msg.channel.send(embed=response)

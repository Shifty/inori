"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import asyncio

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse
from sigma.modules.minigames.utils.ongoing.ongoing import Ongoing
from sigma.modules.utilities.tools.custom.channelactivity import get_history, make_init_message


async def process_channel(channel, me, time_diff):
    """
    Counts total messages and unique users in a test channel.
    :type channel: discord.GuildChannel
    :type me: discord.Member
    :type time_diff: datetime.datetime
    """
    limit_reached = False
    me_perms = channel.permissions_for(me)
    if me_perms.read_messages and me_perms.read_message_history:
        count, unique = await get_history(channel, 100000, time_diff)
        value = f'Total Messages: {count}\nUnique Users: {unique}'
        if count >= 100000 and not limit_reached:
            limit_reached = True
    else:
        value = 'Cannot read channel.\nMissing permissions.'
    name = channel.name if len(channel.name) <= 25 else f'{channel.name[:22]}...'
    return f'#{name}', value, limit_reached


async def categoryactivity(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if not Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
        if not pld.args:
            lookup = str(pld.msg.channel.category.id)
            limit = "7"
        else:
            lookup = pld.args[0] if len(pld.args) > 1 else str(pld.msg.channel.category.id)
            limit = pld.args[-1]
        if lookup.isdigit():
            category = discord.utils.find(lambda x: x.id == int(lookup), pld.msg.guild.categories)
            if category:
                if limit.isdigit() and limit in [str(i) for i in range(1, 8)]:
                    Ongoing.set_ongoing(cmd.name, pld.msg.guild.id)
                    init_message = await make_init_message(pld.msg.channel)
                    time_diff = arrow.get(arrow.utcnow().int_timestamp - (int(limit) * 86400)).naive
                    response = discord.Embed(color=0x744eaa, title=f'{category.name} Category Activity')
                    text_channels = [c for c in category.channels if isinstance(c, discord.TextChannel)]
                    me = category.guild.me
                    tasks = []
                    for channel in text_channels:
                        tasks.append(process_channel(channel, me, time_diff))
                    results = await asyncio.gather(*tasks)
                    footer_added = False
                    for result in results:
                        name, value, limit_reached = result
                        if limit_reached and not footer_added:
                            response.set_footer(text='Limit of 100000 messages.')
                            footer_added = True
                        response.add_field(name=name, value=value)
                    await init_message.edit(embed=response)
                    if Ongoing.is_ongoing(cmd.name, pld.msg.guild.id):
                        Ongoing.del_ongoing(cmd.name, pld.msg.guild.id)
                    return
                else:
                    response = GenericResponse('Limit must be a number 1-7.').error()
            else:
                response = GenericResponse('Category not found.').not_found()
        else:
            response = GenericResponse('Invalid category ID.').error()
    else:
        response = GenericResponse('There is already one ongoing.').error()
    await pld.msg.channel.send(embed=response)

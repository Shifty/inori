"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.generic_responses import GenericResponse


async def hyperlink(_cmd, pld):
    """
    :param _cmd: The command object referenced in the command.
    :type _cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    await pld.msg.delete()
    if pld.args:
        if len(pld.args) >= 2:
            results = []
            all_qry = ' '.join(pld.args)
            hyp_qrys = all_qry.split('|')
            for qry in hyp_qrys:
                if ';' in qry:
                    text = qry.split(';')[0].strip()
                    url = qry.split(';')[1].strip()
                    if "http" in url:
                        if ' ' not in url:
                            results.append(f'[{text}]({url})')
                        else:
                            results.append('**Error:** URL cannot contain spaces')
                    else:
                        results.append('**Error:** URL must include http/s')
                else:
                    results.append("**Error:** Separate the text from URL with '; '")
            response = discord.Embed(color=0x0F80A6, description='\n'.join(results))
            response.set_author(name='Hyperlink', icon_url='https://i.imgur.com/tUo5WrQ.png')
        else:
            response = GenericResponse('Invalid number of arguments').error()
    else:
        response = GenericResponse('Nothing inputted').error()
    await pld.msg.channel.send(embed=response)

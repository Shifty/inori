"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse

ballot_icon = 'https://i.imgur.com/mnJCbFO.png'


async def viewballot(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.args:
        balid = pld.args[0].lower()
        lookup = {'server': pld.msg.guild.id, 'id': balid}
        ballot_data = await cmd.db.col.Ballots.find_one(lookup)
        if ballot_data:
            ballot_channel = await cmd.bot.get_channel(ballot_data.get('channel'))
            ballot_author = await cmd.bot.get_user(ballot_data.get('author'))
            shum_time = arrow.get(ballot_data.get('start')).humanize()
            ehum_time = arrow.get(ballot_data.get('end')).humanize()
            channel = f'#{ballot_channel.name}' if ballot_channel else 'Unknown'
            author = f'{ballot_author.name}#{ballot_author.discriminator}' if ballot_author else 'Unknown'
            results = ballot_data.get("results")
            total = ballot_data.get("total")
            ballot_text = f'**Author:** {author}'
            ballot_text += f'\n**Channel:** {channel}'
            ballot_text += f'\n**Created:** {shum_time}'
            if ballot_data.get('Active'):
                ballot_text += f'\n**Ends:** {ehum_time}'
            else:
                ballot_text += f'\n**Results:** {results}\n**Total Votes:** {total}'
            response = discord.Embed(color=0x244A99)
            response.set_author(name=f'Ballot {ballot_data.get("id")}', icon_url=ballot_icon)
            response.description = ballot_text
        else:
            response = GenericResponse('Ballot not found.').not_found()
    else:
        response = GenericResponse('Missing ballot ID.').error()
    await pld.msg.channel.send(embed=response)

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import asyncio

import arrow
import discord

ballot_loop_running = False
ballot_icon = 'https://i.imgur.com/mnJCbFO.png'


async def ballot_clockwork(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    global ballot_loop_running
    if not ballot_loop_running:
        ballot_loop_running = True
        ev.bot.loop.create_task(ballot_cycler(ev))


def get_percentage(yes, no):
    """
    :type yes: int
    :type no: int
    :rtype: str
    """
    if yes + no == 0:
        percentage = 'No votes.'
    elif yes == 0:
        percentage = '0% in favor.'
    elif no == 0:
        percentage = '100% in favor'
    else:
        percentage = f'{round((yes / (yes + no)) * 100, 2)}% in favor.'
    return percentage


async def ballot_cycler(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    ballot_coll = ev.db.col.Ballots
    while True:
        if ev.bot.is_ready():
            # noinspection PyBroadException
            try:
                now = arrow.utcnow().int_timestamp
                ballot = await ballot_coll.find_one({'end': {'$lt': now}, 'active': True})
                if ballot:
                    await ballot_coll.update_one(ballot, {'$set': {'active': False}})
                    cid = ballot.get('channel')
                    aid = ballot.get('author')
                    mid = ballot.get('message')
                    bid = ballot.get('id')
                    channel = await ev.bot.get_channel(cid)
                    if channel:
                        message = await channel.fetch_message(mid)
                        if message:
                            yes = 0
                            no = 0
                            arw = ['⬆', '⬇']
                            reactions = message.reactions
                            for reaction in reactions:
                                async for user in reaction.users():
                                    if not user.bot:
                                        if reaction.emoji in arw:
                                            if reaction.emoji == arw[0]:
                                                yes += 1
                                            elif reaction.emoji == arw[1]:
                                                no += 1
                            percentage = get_percentage(yes, no)
                            ballot_data = {'results': f'{percentage}', 'total': yes + no}
                            await ballot_coll.update_one({'id': bid}, {'$set': ballot_data})
                            bal_text = f'<@{aid}>, your poll has ended.'
                            bal_title = f'Results - {percentage}'
                            bal_embed = discord.Embed(color=0x244A99)
                            bal_embed.set_author(name=bal_title, icon_url=ballot_icon)
                            await channel.send(bal_text, embed=bal_embed)
            except Exception:
                pass
        await asyncio.sleep(1)

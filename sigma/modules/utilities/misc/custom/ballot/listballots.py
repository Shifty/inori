"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse

ballot_icon = 'https://i.imgur.com/mnJCbFO.png'


async def listballots(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    lookup = {'server': pld.msg.guild.id}
    ballot_docs = await cmd.db.col.Ballots.find(lookup).to_list(None)
    if ballot_docs:
        ballot_lines = []
        for ballot_doc in ballot_docs:
            ballot_channel = await cmd.bot.get_channel(ballot_doc.get('channel'))
            location = f'in **#{ballot_channel.name}**' if ballot_channel else '**in an unknown location**'
            hum_time = arrow.get(ballot_doc.get('end')).humanize()
            results = ballot_doc.get("results")
            if ballot_doc.get('active'):
                status = f'**Ends:** in {hum_time}'
            else:
                status = f'**Results:** {results}' if results else '**Results:** no votes'
            ballot_line = f'`{ballot_doc.get("id")}` {location} - {status}'
            ballot_lines.append(ballot_line)
        outlist = '\n'.join(ballot_lines)
        response = discord.Embed(color=0x244A99)
        response.set_author(name='Ballots', icon_url=ballot_icon)
        response.description = outlist
    else:
        response = GenericResponse('There are no ballots.').not_found()
    await pld.msg.channel.send(embed=response)

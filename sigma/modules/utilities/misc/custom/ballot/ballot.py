"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import secrets

import arrow
import discord

from sigma.core.utilities.data_processing import convert_to_seconds
from sigma.core.utilities.generic_responses import GenericResponse

ballot_icon = 'https://i.imgur.com/mnJCbFO.png'


async def ballot(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.args:
        time_input = pld.args[0]
        try:
            upper_limit = 7776000
            time_sec = convert_to_seconds(time_input)
            if time_sec <= upper_limit:
                start_stamp = arrow.utcnow().int_timestamp
                end_stamp = start_stamp + time_sec
                if time_sec < 90:
                    end_hum = f'in {time_sec} seconds'
                else:
                    end_hum = arrow.get(end_stamp).humanize()
                balid = secrets.token_hex(3)
                ballot_msg = discord.Embed(color=0x244A99)
                ballot_msg.set_author(name='Ballot Box', icon_url=ballot_icon)
                ballot_msg.description = '**Vote by clicking an arrow.**'
                ballot_msg.set_footer(text=f'[{balid}] Ends {end_hum}.')
                ballot_message = await pld.msg.channel.send(embed=ballot_msg)
                [await ballot_message.add_reaction(r) for r in ['⬆', '⬇']]
                ballot_data = {
                    'server': pld.msg.guild.id,
                    'channel': pld.msg.channel.id,
                    'message': ballot_message.id,
                    'author': pld.msg.author.id,
                    'active': True,
                    'start': start_stamp,
                    'end': end_stamp,
                    'id': balid,
                }
                await cmd.db.col.Ballots.insert_one(ballot_data)
                response = None
            else:
                response = GenericResponse('Reminders have a limit of 90 days.').error()
        except (LookupError, ValueError):
            response = GenericResponse('Please use the format HH:MM:SS.').error()
    else:
        response = GenericResponse('No duration inputted.').error()
    if response:
        await pld.msg.channel.send(embed=response)

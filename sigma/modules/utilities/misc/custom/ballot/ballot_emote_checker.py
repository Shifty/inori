"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord


async def check_emotes(bot, msg):
    """
    :type bot: sigma.core.sigma.ApexSigma
    :type msg: discord.Message
    """
    bid = bot.user.id
    present_emoji = []
    togglers = ['⬆', '⬇']
    for reaction in msg.reactions:
        if reaction.emoji in togglers:
            present_emoji.append(reaction.emoji)
        async for emoji_author in reaction.users():
            if emoji_author.id != bid:
                await msg.remove_reaction(reaction.emoji, emoji_author)
    for toggler in togglers:
        if toggler not in present_emoji:
            await msg.add_reaction(toggler)


async def ballot_emote_checker(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.RawReactionPayload
    """
    payload = pld.raw
    uid = payload.user_id
    cid = payload.channel_id
    mid = payload.message_id
    channel = await ev.bot.get_channel(cid)
    try:
        guild = channel.guild
    except AttributeError:
        guild = None
    if guild:
        ballot_coll = ev.db.col.Ballots
        ballot_data = await ballot_coll.find_one({'message': mid, 'active': True})
        if ballot_data:
            user = discord.utils.find(lambda u: u.id == uid and u.guild.id == guild.id, guild.members)
            if user and not user.bot:
                message = await channel.fetch_message(mid)
                if ev.event_type == 'raw_reaction_add':
                    try:
                        await check_emotes(ev.bot, message)
                    except (discord.NotFound, discord.Forbidden):
                        pass

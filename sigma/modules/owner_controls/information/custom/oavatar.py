"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.core.utilities.data_processing import get_image_colors, user_avatar
from sigma.core.utilities.generic_responses import GenericResponse
from sigma.modules.utilities.misc.other.edgecalculator import hexify_int


async def oavatar(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    static = False
    display = False
    if pld.args:
        if pld.args[-1].lower() == 'static':
            pld.args.pop(-1)
            static = True
        if pld.args and pld.args[-1].lower() == 'display':
            pld.args.pop(-1)
            display = True
        lookup = pld.args[0].lower()
        if '#' in lookup:
            uname = lookup.split('#')[0].lower()
            udisc = lookup.split('#')[1]
            target = discord.utils.find(lambda u: u.name.lower() == uname and u.discriminator == udisc, cmd.bot.users)
        else:
            try:
                target = await cmd.bot.get_user(int(lookup))
            except ValueError:
                target = None
        if target:
            ava_url = user_avatar(target, static, display)
            color = await get_image_colors(ava_url)
            response = discord.Embed(color=color)
            response.description = f'Dominant Color: #{hexify_int(color)}'
            response.set_image(url=ava_url)
        else:
            response = GenericResponse('User not found.').not_found()
    else:
        response = GenericResponse('Nothing inputted.').error()
    await pld.msg.channel.send(embed=response)

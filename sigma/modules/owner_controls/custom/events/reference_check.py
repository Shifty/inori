"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import re


def fix_name(name):
    """
    :type name: str
    :rtype: str
    """
    return name.replace('kud', 'kyo')


def fix_usage(usage):
    """
    :type usage: str
    :rtype: str
    """
    usage = re.sub(r'[Ss]igma(\W?)', r'Kon\1', usage)
    usage = re.sub(r'[Kk]ud(\W?)', r'Kyo\1', usage)
    return usage


def fix_desc(desc):
    """
    :type desc: str
    :rtype: str
    """
    desc = re.sub(r'[Ss]igma(\W?)', r'Kon\1', desc)
    desc = re.sub(r'[Kk]ud(\W?)', r'Kyo\1', desc)
    return desc


def fix_alts(cmd, alts):
    """
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :type alts: str
    :rtype: list
    """
    for alt in alts:
        if 'kud' in alt:
            new_alt = alt.replace('kud', 'kyo')
            cmd.bot.modules.alts.pop(alt)
            cmd.bot.modules.alts.update({new_alt: cmd.name})
    return [alt.replace('kud', 'kyo') for alt in alts]


def fix_cmd(cmd):
    """
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :rtype: str, str, str, list
    """
    new_name = ('name', fix_name(cmd.name))
    new_usage = ('usage', fix_usage(cmd.usage))
    new_desc = ('desc', fix_desc(cmd.desc))
    new_alts = ('alts', fix_alts(cmd, cmd.alts))
    return new_name, new_usage, new_desc, new_alts


def replace_ref(cmd, key, item):
    """
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :type key: str
    :type item: str
    :rtype: int
    """
    val = getattr(cmd, key)
    changed = 0
    if val != item:
        setattr(cmd, key, item)
        changed = 1
    return changed


async def reference_check(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    if ev.bot.is_ready():
        changed = 0
        for cmd in ev.bot.modules.commands:
            if cmd != 'keyvis':
                command = ev.bot.modules.commands[cmd]
                for key, item in fix_cmd(command):
                    changed += replace_ref(command, key, item)
        ev.bot.rc_done = True
        ev.log.info(f'Replaced {changed} Command References.')

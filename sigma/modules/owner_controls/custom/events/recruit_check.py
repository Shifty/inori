"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.modules.owner_controls.custom.events.nodes.event_data import EventData

title = 'Welcome to {guild}!\n\n'
info = 'If you wish to join one of our clans, reply to this message with `/apply`.\n'
info += 'Make sure you select the command through this bot.\n'
info += 'Once you\'ve applied, please wait for a recruiter to contact you.'
img_url = 'https://i.imgur.com/Pw4vznp.png'


async def recruit_check(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.MemberUpdatePayload
    """
    dev_mode = ev.bot.cfg.pref.dev_mode
    if not dev_mode:
        data = EventData(ev.bot.cfg.ggl)
        before_role_ids = [role.id for role in pld.before.roles]
        added_role = discord.utils.find(lambda role: role.id not in before_role_ids, pld.after.roles)
        if added_role:
            if added_role.id == data.recruit:
                response = discord.Embed(color=0x3B88C3, description=info)
                guild_icon = str(pld.after.guild.icon.url) if pld.after.guild.icon else None
                response.set_author(name=title.format(guild=pld.after.guild.name), icon_url=guild_icon)
                response.set_image(url=img_url)
                try:
                    await pld.after.send(embed=response)
                except (discord.Forbidden, discord.HTTPException):
                    pass

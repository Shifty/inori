"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from config.google.clan_data import clan_data

from sigma.modules.owner_controls.custom.google.spreadsheet import Sheet


class Clan(object):
    def __init__(self, name, data):
        """
        :type name: str
        :type data: dict
        """
        self.raw = data
        self.name = name
        self.channel = self.raw.get('channel')
        self.role = self.raw.get('role')
        self.r4 = self.raw.get('r4')
        self.recruiter = self.raw.get('recruiter')
        self.misc = self.raw.get('misc', [])
        self.add = self.raw.get('add')

    @property
    def roles(self):
        """
        :rtype: list
        """
        return [self.role, self.r4, self.recruiter] + self.misc


class EventData(object):
    def __init__(self, cfg):
        """
        :type cfg: sigma.core.mechanics.config.GoogleConfig
        """
        self.cfg = cfg
        self.raw = clan_data
        self.channel = 425334362314375179
        self.recruit = 407998945239760911
        self.guest = 407998389834481665

    @property
    def clans(self):
        """
        :rtype: list
        """
        clans = []
        for key, value in self.raw.items():
            clans.append(Clan(key, value))
        return clans

    def get_clan(self, clan):
        """
        :type clan: str
        :rtype: Clan
        """
        data = self.raw.get(clan)
        return Clan(clan, data)

    @property
    def channels(self):
        """
        :rtype: list
        """
        channels = []
        for item in self.raw.values():
            chn = item.get('channel')
            channels.append(chn) if chn else None
        return channels

    @property
    def roles(self):
        """
        :rtype: list
        """
        roles = []
        for item in self.raw.values():
            rl = item.get('role')
            roles.append(rl) if rl else None
        return roles

    @staticmethod
    def ban_check(sheet, ign, mentions, return_full_data):
        """
        :type sheet: config.google.spreadsheet.Sheet
        :type ign: str
        :type mentions: list
        :type return_full_data: bool
        :rtype: bool
        """
        full_data = [u for u in sheet.read('A2:H').values if u]
        users = [u[0].lower().strip() for u in full_data]
        banned, mention = False, None
        if mentions:
            mention_name = mentions[0].name.lower()
            mention_nick = mentions[0].display_name.lower()
            if mention_name in users:
                banned, mention = True, mention_name
            elif mention_nick in users:
                banned, mention = True, mention_nick
        content = ign.replace('`', '')
        name = content.split()[0].lower()
        is_banned = banned or name in users
        if return_full_data:
            user_data = None
            name = mention or name
            if is_banned:
                user_index = users.index(name)
                user_data = full_data[user_index]
            return name in users, user_data
        return is_banned

    def banned(self, ign, mentions, return_full_data=False):
        """
        :type ign: srt
        :type mentions: list
        :type return_full_data: bool
        :rtype: bool or None
        """
        # noinspection PyUnresolvedReferences
        sheet = Sheet(self.cfg.banned)
        if sheet:
            try:
                return self.ban_check(sheet, ign, mentions, return_full_data)
            except AttributeError:
                return None
        return None

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sigma.modules.owner_controls.custom.events.nodes.event_data import EventData

clans = ['rd', 'rda', 'rdu', 'rdr', 'rdi', 'rdp', 'rdl']


def get_clan(content, user_id):
    """
    :type content: str
    :type user_id: str
    :rtype: str
    """
    user_clan = None
    for line in content.split('\n'):
        if user_id in line:
            parts = line.lower().split(' - ')
            try:
                if len(parts[-2]) == 3:
                    parts.pop()
            except IndexError:
                pass
            for clan in clans:
                if clan in parts:
                    user_clan = clan.upper()
                    break
    return user_clan


async def outgoing_check(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.MessagePayload
    """
    dev_mode = ev.bot.cfg.pref.dev_mode
    if not dev_mode:
        data = EventData(ev.bot.cfg.ggl)
        if pld.msg.channel.id == data.channel:
            if pld.msg.mentions:
                for user in pld.msg.mentions:
                    if user.id != pld.msg.author.id and not user.bot:
                        user_clan = get_clan(pld.msg.content, str(user.id))
                        if user_clan:
                            clan = data.get_clan(user_clan)
                            # noinspection PyBroadException
                            try:
                                rem_roles = []
                                for role in clan.roles:
                                    if role in [r.id for r in user.roles]:
                                        rem_role = pld.msg.guild.get_role(role)
                                        rem_roles.append(rem_role)
                                first_add_role = pld.msg.guild.get_role(data.guest)
                                second_add_role = pld.msg.guild.get_role(clan.add) if clan.add else None
                                await user.remove_roles(*rem_roles, reason='Left Clan')
                                await user.add_roles(first_add_role, reason='Left Clan')
                                if second_add_role:
                                    await user.add_roles(second_add_role, reason='Left Clan')
                            except Exception:
                                pass

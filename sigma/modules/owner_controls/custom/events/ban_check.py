"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from sigma.modules.owner_controls.custom.events.nodes.event_data import EventData


async def ban_check(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.MessagePayload
    """
    dev_mode = ev.bot.cfg.pref.dev_mode
    if not dev_mode:
        data = EventData(ev.bot.cfg.ggl)
        if pld.msg.channel.id in data.channels:
            if data.banned(pld.msg.content, pld.msg.mentions):
                # noinspection PyBroadException
                try:
                    await pld.msg.add_reaction('⛔')
                except Exception:
                    pass

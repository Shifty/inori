"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import asyncio

import arrow
import discord

from sigma.core.utilities.url_processing import aioget

api_url = 'https://aftershock-marvel.cdn.prismic.io/api/v2/documents/search?page=1&pageSize=20&lang=en-us'
api_url += '&orderings=[document.first_publication_date desc]&q=[[at(document.type, "update")]]&ref='
ref_url = 'https://aftershock-marvel.prismic.io/api/v2'
web_url = 'https://www.marvelstrikeforce.com/en/updates/'

msf_loop_running = False


async def msf_clockwork(ev):
    """
    :param ev:
    :type ev:
    """
    global msf_loop_running
    if not msf_loop_running:
        msf_loop_running = True
        ev.bot.loop.create_task(msf_cycler(ev))


async def send_news_message(data, channel, role):
    """
    :type data: dict
    :type channel: discord.Channel
    :type role: discord.Role
    """
    date = arrow.get(data['date']).datetime
    msf_embed = discord.Embed(color=0x0764b4, title=data['title'], url=data['url'], timestamp=date)
    msf_embed.set_image(url=data['image'])
    mention = role.mention if role else None
    await channel.send(mention, embed=msf_embed)


async def get_ref():
    """
    :rtype: str
    """
    resp = await aioget(ref_url, True)
    try:
        master_ref = None
        refs = resp.get('refs', [])
        for ref in refs:
            if ref['isMasterRef']:
                master_ref = ref['ref']
                break
    except KeyError:
        master_ref = None
    return master_ref


async def msf_cycler(ev):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    """
    dev_mode = ev.bot.cfg.pref.dev_mode
    if dev_mode:
        return
    while True:
        if ev.bot.is_ready():
            channel = await ev.bot.get_channel(594376253956423680)
            if channel:
                role = channel.guild.get_role(594379155055968284)
                api_ref = await get_ref()
                if api_ref:
                    news_data = await aioget(api_url + api_ref, True)
                    if news_data:
                        news_items = reversed(news_data.get('results', []))
                        for news in news_items:
                            try:
                                db_check = await ev.db.col.MSFCache.find_one({'event_id': news['id']})
                                if not db_check:
                                    news_data = {
                                        'event_id': news['id'],
                                        'url': web_url + news['uid'],
                                        'date': news['first_publication_date'],
                                        'title': news['data']['title'][0]['text'],
                                        'image': news['data']['featured_image']['url']
                                    }
                                    await send_news_message(news_data, channel, role)
                                    await ev.db.col.MSFCache.insert_one({'event_id': news['id']})
                            except KeyError:
                                pass
                await asyncio.sleep(1)
            await asyncio.sleep(300)

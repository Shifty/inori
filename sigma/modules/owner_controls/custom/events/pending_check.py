"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import discord

from sigma.modules.owner_controls.custom.events.nodes.event_data import EventData


async def pending_check(ev, pld):
    """
    :param ev: The event object referenced in the event.
    :type ev: sigma.core.mechanics.event.SigmaEvent
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.MessagePayload
    """
    dev_mode = ev.bot.cfg.pref.dev_mode
    if not dev_mode:
        data = EventData(ev.bot.cfg.ggl)
        if pld.msg.mentions:
            user = pld.msg.mentions[0]
            for clan in data.clans:
                if clan.channel == pld.msg.channel.id:
                    try:
                        rem_role_r = pld.msg.guild.get_role(data.recruit)
                        rem_role_g = pld.msg.guild.get_role(data.guest)
                        add_role = pld.msg.guild.get_role(clan.role)
                        await user.remove_roles(rem_role_r, rem_role_g, reason='Joined Clan')
                        await user.add_roles(add_role, reason='Joined Clan')
                    except (discord.NotFound, discord.Forbidden):
                        pass

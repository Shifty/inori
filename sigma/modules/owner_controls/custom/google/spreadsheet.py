"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

# noinspection PyPackageRequirements

from googleapiclient import discovery, errors
from httplib2 import Http
from oauth2client import client, file, tools

SCOPE = 'https://www.googleapis.com/auth/spreadsheets'
SECRET_PATH = 'sigma/modules/owner_controls/custom/google/client_secret.json'
TOKEN_PATH = 'sigma/modules/owner_controls/custom/google/token.json'


class Sheet(object):
    """
    Wrapper for a Google spreadsheet
    """

    def __init__(self, sheet_id):
        """
        :type sheet_id: str
        """
        self.sheet = self._document()
        self._sheet_id = sheet_id

    @property
    def _credentials(self):
        """
        :rtype: oauth2client.client.Credentials
        """
        store = file.Storage(TOKEN_PATH)
        creds = store.get()
        if not creds or creds.invalid:
            flow = client.flow_from_clientsecrets(SECRET_PATH, SCOPE)
            creds = tools.run_flow(flow, store)
        return creds

    def _document(self):
        """
        :rtype: googleapiclient.discovery.Resource
        """
        service = discovery.build('sheets', 'v4', http=self._credentials.authorize(Http()))
        return service.spreadsheets().values()

    def read(self, ranges, cols=False):
        """
        :type ranges: str
        :type cols: bool
        """
        print(type(self.sheet))
        try:
            dimensions = 'COLUMNS' if cols else 'ROWS'
            request = self.sheet.get(spreadsheetId=self._sheet_id, range=ranges, majorDimension=dimensions).execute()
            setattr(self, 'values', request.get('values', []))
            return self
        except errors.HttpError:
            return None

    def write(self, ranges, values, cols=False):
        """
        :type ranges: str
        :type values: list
        :type cols: bool
        """
        dimensions = 'COLUMNS' if cols else 'ROWS'
        body = {'values': values, 'majorDimension': dimensions}
        self.sheet.update(spreadsheetId=self._sheet_id, range=ranges, valueInputOption='RAW', body=body).execute()

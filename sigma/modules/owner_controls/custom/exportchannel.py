"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

import arrow
import discord

from sigma.core.utilities.generic_responses import GenericResponse


async def get_history(channel, limit):
    """
    :type channel: discord.GuildChannel
    :type limit: int
    :rtype: list
    """
    history = []
    channel_hist = [m async for m in channel.history(limit=limit)]
    for log in channel_hist:
        content = log.content if log.content else '[No Text Content]'
        stamp = log.created_at
        log_line = f'[{stamp.date()} {str(stamp.time())[:5]} | {log.author.name}]\n{content}\n'
        if log.attachments:
            attachments = ', '.join([a.url for a in log.attachments])
            log_line += f'Attachments: [{attachments}]\n'
        history.insert(0, log_line)
    return history


def make_export_file(channel, history):
    """
    :type channel: discord.GuildChannel
    :type history: list
    """
    if not os.path.exists('exports'):
        os.makedirs('exports')
    with open(f'exports/{channel.guild.name} {channel.name}.txt', 'w', encoding='utf-8') as export_file:
        info_lines = f'Server: {channel.guild.name} [{channel.guild.id}]\n'
        info_lines += f'Channel: {channel.name} [{channel.id}]\n'
        info_lines += f'Messages: {len(history)}\n'
        info_lines += f'Date: {arrow.utcnow().format("DD. MMMM YYYY HH:mm:ss")} UTC\n'
        info_lines += f'{"=" * 40}\n\n'
        export_file.write(info_lines)
        export_file.write('\n'.join(history))


async def exportchannel(cmd, pld):
    """
    :param cmd: The command object referenced in the command.
    :type cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    if pld.args:
        target = pld.args[0]
        if target.isdigit():
            limit = pld.args[-1] if len(pld.args) > 1 else None
            if limit is None or limit.isdigit():
                if limit is not None:
                    limit = int(limit)
                channel = await cmd.bot.get_channel(int(target))
                if isinstance(channel, discord.TextChannel):
                    me = channel.guild.me
                    if channel.permissions_for(me).read_messages and channel.permissions_for(me).read_message_history:
                        start = arrow.utcnow().int_timestamp
                        init_response = discord.Embed(color=0x744eaa, title=f'📑 Indexing {channel.name}...')
                        init_message = await pld.msg.channel.send(embed=init_response)
                        history = await get_history(channel, limit)
                        make_export_file(channel, history)
                        done_response = discord.Embed(color=0x744eaa, title=f'📑 Finished indexing {channel.name}.')
                        done_response.set_footer(text=f'Elapsed Time: {arrow.utcnow().int_timestamp - start} seconds')
                        await init_message.edit(embed=done_response)
                        try:
                            await pld.msg.author.send(embed=done_response)
                        except (discord.Forbidden, discord.HTTPException):
                            pass
                        return
                    else:
                        response = GenericResponse('I can\'t read that channel\'s history.').error()
                else:
                    response = GenericResponse('Target must be a text channel.').error()
            else:
                response = GenericResponse('Limit must be a number.').error()
        else:
            response = GenericResponse('Invalid channel ID.').error()
    else:
        response = GenericResponse('Nothing inputted.').error()
    await pld.msg.channel.send(embed=response)

"""
Apex Sigma: The Database Giant Discord Bot.
Copyright (C) 2019  Lucia's Cipher

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import os
import re

import discord

from sigma.core.utilities.generic_responses import GenericResponse


def get_file(cid):
    """
    :type cid: str
    :rtype: str
    """
    target = None
    for root, dirs, files in os.walk('exports'):
        for file in files:
            with open(f'exports/{file}', 'r', encoding='utf-8') as export_file:
                line = export_file.readlines(1000)[1]
                # matches a channel ID in brackets
                if re.search(r'\[' + re.escape(cid) + r']', line):
                    target = file
                    break
    return target


async def getexport(_cmd, pld):
    """
    :param _cmd: The command object referenced in the command.
    :type _cmd: sigma.core.mechanics.command.SigmaCommand
    :param pld: The payload with execution data and details.
    :type pld: sigma.core.mechanics.payload.CommandPayload
    """
    target_file = None
    if pld.args:
        cid = pld.args[0]
        if cid.isdigit():
            if os.path.exists('exports'):
                target = get_file(cid)
                if target:
                    target_file = discord.File(f'exports/{target}', target)
                    response = None
                else:
                    response = GenericResponse('No export for that channel was found.').error()
            else:
                response = GenericResponse('No export directory was found.').error()
        else:
            response = GenericResponse('Invalid channel ID.').error()
    else:
        response = GenericResponse('Nothing inputted.').error()
    if target_file:
        await pld.msg.channel.send(file=target_file)
    else:
        await pld.msg.channel.send(embed=response)
